getById = (id) => document.getElementById(id);
signUp = () => displayMessage("Sign up clicked!");
forgotPassword = () => displayMessage("Forgot password clicked!");
getFormState = (userName, password, rememberMeClicked) => ({
    userName: userName,
    password: password,
    rememberMeClicked: rememberMeClicked
});

function displayMessage(message) {
    console.log(message);
}

function login() {

    const userName = getById("username").value;
    const password = getById("password").value;
    const rememberMeClicked = getById("rememberMe").checked;

    if (userName === '' || password === '') {
        displayMessage("You must enter a username or password");
    } else {
        const formState = getFormState(userName, password, rememberMeClicked);
        displayMessage(JSON.stringify(formState));
    }

}

